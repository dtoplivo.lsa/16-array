﻿#include <iostream>
#include <time.h>

int main()
{    
    //Определяет номер календарного дня
    time_t now = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &now);
    int day = timeinfo.tm_yday;


    const int N = 4; //Создает константу для размерности  массива
    int array [N][N]; //Создает массив размерности NxN
    for (int i = 0; i < N; i++) //цикл для заполнения элемента с индексом i
    {
        for (int j = 0; j < N; j++) //Цикл для заполнения элемента с индексом j
        {
            array [i][j] = i + j; // Заполнение массива суммой индексов
            std::cout << array[i][j] << "\n";
        }
        
    }
   
    day = day % N; //Присваиват переменной выражение, как в задании
    int sum (0);
    
    for (int j = 0; j < N; j++) 
    {
        sum = sum + array[day][j]; 
    }
    std::cout << sum; //Выводит сумму элементов в строке массива

}
